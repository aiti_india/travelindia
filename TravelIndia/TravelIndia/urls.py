from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'TravelIndia.views.home', name='home'),
    # url(r'^TravelIndia/', include('TravelIndia.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    # Enable the states URLs
    # http://mysite/states/ should invoke the list_states view
    url(r'^states/$', 'places.views.list_states'),
    url(r'^states/json$', 'places.views.list_states_json'),
    # http://mysite/states/[digits] should invoke the show_state view,
    # and the digits should be passed as the argument "state_id" to that view.
    url(r'^states/(?P<state_id>\d+)/$', 'places.views.show_state'),
    url(r'^states/(?P<state_id>\d+)/json$', 'places.views.show_state_json'),
    # Added a URL to view the list of cities. It invokes the list_cities view.
    url(r'^cities/$', 'places.views.list_cities'),
    # Added a URL for form to make new cities. It invokes the new_city view.
    url(r'^cities/new_city/$', 'places.views.new_city'),
    # Added a URL for a confirmation page after the form has been submitted.
    url(r'^cities/new_city/thanks/$', 'places.views.thanks')
)
