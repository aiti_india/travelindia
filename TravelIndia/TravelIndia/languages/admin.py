from languages.models import Language
from django.contrib import admin

admin.site.register(Language)