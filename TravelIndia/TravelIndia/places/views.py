# Create your views here.
from django.template import Context, loader
from django.utils import simplejson as json
from django.core.urlresolvers import reverse
from places.models import State
from places.models import City
from places.models import CityForm
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.core.context_processors import csrf

# Two views for States
def list_states(request):
    """List all states"""
    # Get all states ordered by name
    state_list = State.objects.all().order_by('name')
    # Load the template
    t = loader.get_template('places/states/list.html')
    # Set up the context for the template
    c = Context({
        # The states variable in the template will be set from
        # the state_list variable here.
        'states': state_list
    })
    # Render the template with the context
    data = t.render(c)
    return HttpResponse(data)

def list_states_json(request):
    """List all states and return the value in JSON"""
    state_list = State.objects.all().order_by('name')
    # Map the state_list using a list comprehension
    state_list = [{'name': state.name,
                   'url': reverse('places.views.show_state_json', args=[str(state.id)])}
                  for state in state_list]
    data = json.dumps(state_list)
    content_type = "text/plain" #"application/json"
    return HttpResponse(data, content_type=content_type)

# Do similarly for the detail view.
def show_state(request, state_id):
    """Show the detail for one state. Note that we take one argument!"""
    # Get the state for which we want to show detail
    state = State.objects.get(id=state_id)
    # Prepare the template and return it, as before
    t = loader.get_template('places/states/detail.html')
    c = Context({
        'state': state
    })
    return HttpResponse(t.render(c))

def show_state_json(request, state_id):
    """Show the detail for one state and return the value in JSON"""
    state = State.objects.get(id=state_id)
    # Map the state_list using a list comprehension
    state_data = {'name': state.name,
                  'capital': state.capital,
                  'languages': [language.name for language in state.languages()],
                  'population': state.population,
                  # DecimalField is not directly serializable.
                  'area': float(state.area)}
    data = json.dumps(state_data)
    content_type = "text/plain" #"application/json"
    return HttpResponse(data, content_type=content_type)

def list_cities(request):
    """List all cities"""
    # Get all City objects ordered by name
    city_list = City.objects.all().order_by('name')
    # Load the template
    t = loader.get_template('places/cities/list.html')
    # Set up the context for the template
    c = Context({
        # The cities variable in the template will be set from
        # the city_list variable here.
        'cities': city_list
    })
    # Render the template with the context
    data = t.render(c)
    return HttpResponse(data)

def get_city_by_id(city_id):
    "Return the city object that has the input id"
    city = City.objects.get(id = city_id)
    return city

def get_city_by_name(city_name):
    "Return the city object that has the input name"
    city = City.objects.get(name = city_name)
    return city

def new_city(request):
    if request.method == 'POST':
        form = CityForm(request.POST)
        if form.is_valid():
            city = City()
            city.name=form.cleaned_data["name"]
            state_id = form.cleaned_data["state"]
            city.state = State.objects.get(id = state_id)
            city.population=form.cleaned_data["population"]
            city.latitude=form.cleaned_data["latitude"]
            city.longitude=form.cleaned_data["longitude"]
            city.save()
            # Redirect after POST
            return HttpResponseRedirect('/cities/new_city/thanks')
    else:
        form = CityForm() # An unbound form
    c = {'form': form}
    c.update(csrf(request))
    return render_to_response('places/cities/new_city.html', c)

def thanks(request):
    """Display a thanks message as confirmation that the form was submitted"""
    t = loader.get_template('places/cities/thanks.html')
    # Set up the context for the template
    c = Context({
        # No variables need to be set
    })
    # Render the template with the context
    data = t.render(c)
    return HttpResponse(data)
    