from places.models import State, StateLanguage
from django.contrib import admin

admin.site.register(State)
admin.site.register(StateLanguage)