from django.db import models
from languages.models import Language
from django.core.urlresolvers import reverse
from django import forms

# Create your models here.
class State(models.Model):
    """A State has five properties stored in the database.
    
    name -- The name of the state (in English)
    code -- The 2-letter code of the state
    capital -- The capital of the state (in English)
    population -- The most recent population of the state
                  This is a BigIntegerField because IntegerField
                  can only store up to around 200 crore.
    area -- The area of the state in square kilometers
            max_digits is 11 as the surface of the Earth is
            51 crore square kilometers.
    """
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=2)
    capital = models.CharField(max_length=255)
    population = models.BigIntegerField()
    area = models.DecimalField(max_digits=11, decimal_places=2)
    
    def population_density(self):
        """The population density can be derived from the population and area."""
        return self.population / self.area
    
    def languages(self):
        """Languages are not supported directly.  To get the languages, we need
        to resolve the StateLanguage objects of this state."""
        # Get the StateLanguage objects having this State.
        state_languages = StateLanguage.objects.filter(state=self)
        # Map the list of StateLanguage objects to Language objects.
        
        # This is a list comprehension.  It states that we should make a new
        # list based on the objects in state_languages (it will thus have the
        # same number of entries as there are in state_languages).
        #
        # This list will be constructed using the values of the language property
        # of each object in state_languages (temporarily named using the
        # variable state_language)
        #
        # Thus, you can read this as:
        #
        # Build a new list such that, for each item (named state_language)
        # in state_languages, there is an equivalent item that is the result
        # of evaluating "state_language.language" for that item.
        return [state_language.language for state_language in state_languages]
    
    def __unicode__(self):
        """This should always be provided.  If the state needs to be rendered
        as a string, this is used."""
        return self.name
    
    def get_absolute_url(self):
        """This should be provided if you can.  It allows you to easily get the
        URL for this model's representation."""
        # This looks up how "places.views.state_details" is defined in urls.py
        # and returns the equivalent URL with its ID set by str(self.id).
        #
        # I could also write this function more quickly by writing:
        #
        # @models.permalink
        # def get_absolute_url(self):
        #     return ('places.views.show_state', [str(self.id)])
        return reverse('places.views.show_state', args=[str(self.id)])
    
    def bordering_states(self):
        """Gets list of bordering state names"""
        # Get all BorderingStates objects that have this state as either state1 or state2
        bordering_states_1 = self.bordering_state_as_state1.all()
        bordering_states_2 = self.bordering_state_as_state2.all()
        
        # Create an empty list for storing the bordering state names
        answer = []
        
        # For each BorderingStates object, get the name of the other state
        # and add it to the list
        for pair in bordering_states_1:
            answer.append(pair.state2.name)
        for pair in bordering_states_2:
            answer.append(pair.state1.name)
        return answer    

class StateLanguage(models.Model):
    """Google App Engine cannot support ManyToMany relationships directly.
    Thus, we create a model, StateLanguage, which relates a State to a Language.
    
    state -- The State in the relationship.
    language -- The Language in the relationship.
    """
    state = models.ForeignKey(State)
    language = models.ForeignKey(Language)
    
    def __unicode__(self):
        return self.state.name + ": " + self.language.name
    
class City(models.Model):
    """A City has five properties stored in the database.
    
    name -- The name of the city (in English)
    state -- The State that this city belongs to.
    population -- The most recent population of the city
                  This is a BigIntegerField.
    latitude -- This is a FloatField
    longitude -- This is a FloatField
    """
    name = models.CharField(max_length=255)
    state = models.ForeignKey(State)
    population = models.BigIntegerField()
    latitude = models.FloatField()
    longitude = models.FloatField()
    
    def __unicode__(self):
        return self.name
    
class BorderingStates(models.Model):
    """Stores pairs of states that border each other"""
    
    state1 = models.ForeignKey(State, related_name = "bordering_state_as_state1")
    state2 = models.ForeignKey(State, related_name = "bordering_state_as_state2")
    
class CityForm(forms.Form):
    name = forms.CharField(max_length=255)
    state = forms.ModelMultipleChoiceField(
                queryset=State.objects.all())
    population = forms.IntegerField()
    latitude = forms.FloatField()
    longitude = forms.FloatField()
    
    